gtk-d (3.9.0-3) unstable; urgency=medium

  * No-change source-only upload.

 -- Matthias Klumpp <mak@debian.org>  Wed, 13 Nov 2019 22:39:42 +0100

gtk-d (3.9.0-2) unstable; urgency=medium

  * Set the right linker flag prefix for GDC
  * Simplify d/rules and try to make GDC builds work
    - Hopefully closes: #940717, however it appears like GDC 9
      might have a bug in mixin symbol generation when doing
      split builds. So let's see if this compiles on armhf.
  * Build & install libpeas bindings

 -- Matthias Klumpp <mak@debian.org>  Mon, 30 Sep 2019 00:36:05 +0200

gtk-d (3.9.0-1) unstable; urgency=medium

  * New upstream release: 3.9.0
  * Bump standards version and dh compat level: No changes needed
  * Add a bunch of upstream patches to fix generated pkgconfig files
  * Use GDC if default on the given architecture
  * Simplify d/watch

 -- Matthias Klumpp <mak@debian.org>  Sun, 15 Sep 2019 19:54:16 +0200

gtk-d (3.8.5-1) unstable; urgency=medium

  * New upstream release
  * Drop Update-version-to-3.8.4.patch: Applied in new release

 -- Jeremy Bicha <jbicha@debian.org>  Sun, 06 Jan 2019 16:57:40 -0500

gtk-d (3.8.4-2) unstable; urgency=medium

  [ Pablo De Napoli ]
  * Set DC=ldc2 to fix build when other D compilers are installed

  [ Jeremy Bicha ]
  * Use dh_auto_build instead of calling make directly
  * Add Update-version-to-3.8.4.patch:
    - Proposed patch to correct reported version, needed by tilix 1.8.7

 -- Jeremy Bicha <jbicha@debian.org>  Sat, 05 Jan 2019 10:37:00 -0500

gtk-d (3.8.4-1) unstable; urgency=medium

  * New upstream release

 -- Jeremy Bicha <jbicha@debian.org>  Sun, 25 Nov 2018 16:20:24 -0500

gtk-d (3.8.3-1) unstable; urgency=medium

  * New upstream bugfix release: 3.8.3
  * Build-depend on optional gir-to-d

 -- Matthias Klumpp <mak@debian.org>  Mon, 16 Jul 2018 23:45:36 +0200

gtk-d (3.8.2-1) unstable; urgency=medium

  * New upstream release

 -- Jeremy Bicha <jbicha@debian.org>  Sat, 12 May 2018 17:42:31 -0400

gtk-d (3.7.5-2) unstable; urgency=medium

  * If needed, transform (GCC) LDFLAGS to flags that
    LDC can understand (Closes: #891048)

 -- Matthias Klumpp <mak@debian.org>  Fri, 16 Mar 2018 22:42:00 +0100

gtk-d (3.7.5-1) unstable; urgency=medium

  * New upstream release 3.7.5

 -- Jeremy Bicha <jbicha@debian.org>  Fri, 16 Mar 2018 16:40:58 -0400

gtk-d (3.7.3-1) unstable; urgency=medium

  [ Jeremy Bicha ]
  * Update Vcs fields for migration to https://salsa.debian.org/

  [ Matthias Klumpp ]
  * New upstream release: 3.7.3 (Closes: #889652)
  * Use dh-dlang
  * Use dh compat level 11
  * d/copyright: Use secure URLs

 -- Matthias Klumpp <mak@debian.org>  Thu, 22 Feb 2018 21:44:24 +0100

gtk-d (3.7.1-1) unstable; urgency=medium

  * New upstream version 3.7.1
  * Bump standards version: No changes needed

 -- Matthias Klumpp <mak@debian.org>  Thu, 09 Nov 2017 12:28:48 +0100

gtk-d (3.6.6-1) unstable; urgency=medium

  * New upstream version: 3.6.6
  * Update to dh compat level 10
  * Bump standards version: No changes needed

 -- Matthias Klumpp <mak@debian.org>  Sun, 01 Oct 2017 10:12:46 +0200

gtk-d (3.6.5-2) unstable; urgency=medium

  * Adjust d/copyright
  * Install missing header files

 -- Matthias Klumpp <mak@debian.org>  Thu, 13 Jul 2017 22:00:46 +0200

gtk-d (3.6.5-1) unstable; urgency=medium

  * New upstream version 3.6.5
  * fix-pkgconfig-location.patch: Adjust to new version
  * Use https version of homepage URL
  * Update dependencies
  * Bump standards version: No changes needed

 -- Matthias Klumpp <mak@debian.org>  Thu, 13 Jul 2017 21:40:14 +0200

gtk-d (3.5.1-1) unstable; urgency=medium

  * New upstream version: 3.5.1
  * Drop all patches: Applied upstream
  * fix-pkgconfig-location.patch: Install pkg-config files correctly

 -- Matthias Klumpp <mak@debian.org>  Tue, 24 Jan 2017 23:42:18 +0100

gtk-d (3.3.1-2) unstable; urgency=medium

  * no-bitness-hardcoding.patch: Don't hardcode -m<arch>

 -- Matthias Klumpp <mak@debian.org>  Sun, 17 Jul 2016 19:48:06 +0200

gtk-d (3.3.1-1) unstable; urgency=medium

  * Initial release (Closes: #829678)

 -- Matthias Klumpp <mak@debian.org>  Tue, 12 Jul 2016 14:04:42 +0200
